package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseGuapisimaChile;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class GuapisimaChile extends TestBaseGuapisimaChile {
	
	final WebDriver driver;
	public GuapisimaChile(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInGuapisimaChile(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Guapisima Chile - landing");
		
		WebElement menuOpen = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen.click();
			espera(500);
				WebElement menuSection1 = driver.findElement(By.xpath("//a[contains(text(), 'Belleza')]"));
				menuSection1.click();
					String elemento1 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento1,"Belleza");
					System.out.println(elemento1);
			espera(500);
		
			
		WebElement menuOpen2 = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen2.click();
			espera(500);
				WebElement menuSection2 = driver.findElement(By.xpath("//a[contains(text(), 'Moda')]"));
				menuSection2.click();
					String elemento2 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento2,"Moda");
					System.out.println(elemento2);
			espera(500);
			
			
		WebElement menuOpen3 = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen3.click();
			espera(500);
				WebElement menuSection3 = driver.findElement(By.xpath("//a[contains(text(), 'Deco')]"));
				menuSection3.click();
					String elemento3 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento3,"Deco");
					System.out.println(elemento3);
			espera(500);
			
		
		WebElement menuOpen4 = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen4.click();
			espera(500);
				WebElement menuSection4 = driver.findElement(By.xpath("//a[contains(text(), 'Cocina')]"));
				menuSection4.click();
					String elemento4 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento4,"Cocina");
					System.out.println(elemento4);
			espera(500);
		
			
		WebElement menuOpen5 = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen5.click();
			espera(500);
				WebElement menuSection5 = driver.findElement(By.xpath("//a[contains(text(), 'Agenda')]"));
				menuSection5.click();
					String elemento5 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento5,"Agenda");
					System.out.println(elemento5);
			espera(500);
			
			
		WebElement menuOpen6 = driver.findElement(By.cssSelector(".menu-open"));
			menuOpen6.click();
			espera(500);
				WebElement menuSection6 = driver.findElement(By.xpath("//a[contains(text(), 'Historias')]"));
				menuSection6.click();
					String elemento6 = driver.findElement(By.cssSelector(".col-12.col-md-12")).getText();
					Assert.assertEquals(elemento6,"Historias");
					System.out.println(elemento6);
			espera(500);
		

		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Guapisima Chile"); 
		System.out.println();
		System.out.println("Fin de Test Guapisima Chile - Landing");
			
	}			

	
}  

